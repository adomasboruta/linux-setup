#!/bin/bash

set -eu

sudo apt update
sudo apt install curl
sudo apt install snapd
sudo snap install code --classic

git config --global core.editor "code --wait"
git config --global pull.rebase true

sudo apt remove --purge unclutter-startup

sudo apt install jq

# Office
sudo apt install libreoffice

# Shell prompt
curl -fsSL https://starship.rs/install.sh | bash

# Docker
sudo apt install apt-transport-https ca-certificates curl gnupg lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt install docker-ce docker-ce-cli containerd.io

sudo usermod -aG docker $USER

echo "You may need to restart your PC for getting docker work without sudo"
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo curl -L https://raw.githubusercontent.com/docker/compose/1.29.2/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose

# Increase max number of file watches
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p

curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

sudo apt install build-essential

sudo apt-get install clamav clamav-daemon
