#!/bin/bash

set -eu

./install-common.sh

sudo snap install teams
sudo snap install authy
sudo apt install network-manager-openvpn network-manager-openvpn-gnome
#snap install intellij-idea-ultimate --classic
#snap install pycharm-professional --classic
#snap install datagrip --classic
sudo apt install dos2unix
